/*
Daniel Vadala
3/5/18
LoopKeys.cpp
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.
*/

#include "LoopKeys.h"


//initializes number of keys to 0 and loop start to N/A
LoopKeys::LoopKeys()
{
	numKeys = 0;
	loopStart = "N/A";
}


LoopKeys::~LoopKeys()
{
}

//adds keys to keys array
void LoopKeys::addKey(string newKey)
{
	keys[numKeys] = newKey;
	numKeys++;
}

//loops through keys to find match
bool LoopKeys::keyMatch(string newKey)
{
	for (int i = 0; i < numKeys; i++)
	{
		if (newKey == keys[i])
		{
			return true;
		}
	}
	return false;
}

//outputs keys found in prime number
void LoopKeys::outPutKeys()
{
	//flag to show start of loop
	bool showStart = false;

	//loop through printing keys found
	for (int i = 0; i < numKeys; i++)
	{
		//check where loop starts and output to user
		if (!showStart && keys[i] == loopStart)
		{
			showStart = true;
			cout << endl << "Loop Starts Here" << endl;
			cout << "key " << i << ": " << keys[i] << endl;
		}
		else
		{
			cout << "key " << i << ": " << keys[i] << endl;
		}
	}
}
