/*
Daniel Vadala
3/5/18
main.cpp
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.
*/
#include <iostream>
#include <cstdlib>
#include "LoopKeys.h"
#include "LargePrime.h"




using namespace std;

bool findLoop(LargePrime*, string);

const int LOOP_CUTOFF = 100;


int main()
{

	//Random Test Strings
	LargePrime* testPrime1 = new LargePrime("pi1000000.txt");
	LargePrime* testPrime2 = new LargePrime("e.2mil.txt");
	LargePrime* testPrime3 = new LargePrime("M77232917.txt");


	bool UIFlag = true;
	string userSeed;
	string userPrimeChoice;

	//Test "UI"
	while (UIFlag)
	{
		cout << "Prime 1 is PI 2 is e 3 is M77232917" << endl;
		cout << "Enter Prime to use (TEST 1, 2, or 3): ";
		cin >> userPrimeChoice;

		if (userPrimeChoice == "3")
		{
			cout << "Enter Seed Number: ";
			cin >> userSeed;
			cout << endl;
			//check if for loops in prime
			if (findLoop(testPrime3, userSeed))
			{
				cout << endl << "Loop Found" << endl;
			}
			else
			{
				cout << endl << "No Loop Found" << endl;
			}
		}
		else if (userPrimeChoice == "2")
		{
			cout << "Enter Seed Number: ";
			cin >> userSeed;
			cout << endl;
			//check if for loops in prime
			if (findLoop(testPrime2, userSeed))
			{
				cout << endl << "Loop Found" << endl;
			}
			else
			{
				cout << endl << "No Loop Found" << endl;
			}
		}
		else if (userPrimeChoice == "1")
		{
			cout << "Enter Seed Number: ";
			cin >> userSeed;
			cout << endl;
			//check if for loops in prime
			if (findLoop(testPrime1, userSeed))
			{
				cout << endl << "Loop Found" << endl;
			}
			else
			{
				cout << endl << "No Loop Found" << endl;
			}
		}
		else if (userPrimeChoice == "q" || userPrimeChoice == "Q")
		{
			UIFlag = false;
		}
		else
		{
			cout << "Invalid Input" << endl << endl;
		}
		cout << endl << endl;
	}




	return 0;
}

//Search Prime number for a loop
bool findLoop(LargePrime* currentPrime, string seed)
{
	cout << "FINDING LOOP" << endl;
	//flags for if a key or loop itself is found
	bool loopFound = false;
	bool keyFound = false;
	int numKeys = 1;

	//initialize keys object
	LoopKeys* keys = new LoopKeys();

	//set current key to the seed
	string currentKey = seed;
	
	//initialize Current Index that is being searched
	int currentIndex = 1;

	int currentKeyFirstDigit;

	//loop while a loop is not found or whole prime is searched through
	while ((!loopFound && currentIndex < currentPrime->getSize()) && numKeys < LOOP_CUTOFF)
	{
		//reinitialize key found flag and current index for inner loop
		keyFound = false;
		currentIndex = 1;

		//loop through until a key is found or the while prime has been searched
		while (!keyFound && currentIndex < currentPrime->getSize())
		{
			
			//Test Output To Make Sure Program is running properly
			//if (currentIndex % 1000 == 0){
				//cout << currentIndex << endl;
			//}
			
			//Check if current key matches substring of prime at current index
			currentKeyFirstDigit = currentKey[0] - '0';
			//to skip getting substring if first digit doesn't match
			if (currentPrime->digitAt(currentIndex) == currentKeyFirstDigit)
			{
				if (currentPrime->equalSubString(currentKey, currentIndex, currentKey.length()))
				{
					cout << "Current Key: " << currentKey << endl;
					//check if current Key is already in the set of keys  
					if (keys->keyMatch(currentKey))
					{
						cout << "Loop Found On Key: " << currentKey << endl << endl;
						//set key values in key object
						keys->addKey(currentKey);
						keys->setLoopStart(currentKey);
						//set off loop glags
						loopFound = true;
						keyFound = true;
					}
					else
					{
						//set new key in key objects
						keys->addKey(currentKey);
						numKeys++;
						//set new key
						currentKey = to_string(currentIndex);
						//set off inner loop flag
						keyFound = true;
					}
				}

			}
			
			currentIndex++;
		}
	}
	
	
	//check if loop was found or not and return accordingly after outputting keys
	if (loopFound)
	{
		keys->outPutKeys();
		delete keys;
		return true;
	}
	keys->addKey(currentKey);
	keys->outPutKeys();
	delete keys;
	return false;





}