/*
Daniel Vadala
3/5/18
LargePrime.h
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.
*/

#include <string>
#include <fstream>
#include <iostream>

using namespace std;

#pragma once
class LargePrime
{
public:
	LargePrime(string);
	~LargePrime();

	const int MAX_DIGITS = 25000000;

	//return size of prime
	int getSize() { return size; }

	//return digits of prime
	string getDigits() { return digits; }

	//gets a substring at index given and with size given
	string subStringAt(int, int);

	bool equalSubString(string, int, int);

	int digitAt(int index) { return arrayDigits[index]; }

	void readFile(string fileName);

private:

	//stores size of prime
	int size;

	//stores digits of prime
	string digits;

	int arrayDigits[25000000];



};

