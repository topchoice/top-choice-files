/*
Daniel Vadala
3/5/18
LargePrime.h
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.
*/

#include <string>

using namespace std;

#pragma once
class LargePrime
{
public:
	LargePrime(string);
	~LargePrime();

	//return size of prime
	int getSize() { return size; }

	//return digits of prime
	string getDigits() { return digits; }

	//gets a substring at index given and with size given
	string subStringAt(int, int);

private:

	//stores size of prime
	int size;

	//stores digits of prime
	string digits;

};

