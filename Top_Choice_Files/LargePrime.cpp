/*
Daniel Vadala
3/5/18
LargePrime.cpp
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.
*/
#include "LargePrime.h"


//constructor sets digits of LargePrime
LargePrime::LargePrime(string newDigits)
{
	digits = newDigits;
	size = digits.length();

}


LargePrime::~LargePrime()
{
}

//Given an index and size returns substring at that index and returns substring of the size
string LargePrime::subStringAt(int index, int size)
{
	string pieceOfPrime = "";

	for (int i = index; i < index + size; i++)
	{
		pieceOfPrime += digits[i];
	}

	return pieceOfPrime;
}
