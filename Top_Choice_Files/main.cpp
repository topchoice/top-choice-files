/*
Daniel Vadala
3/5/18
main.cpp
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.
*/
#include <iostream>
#include <cstdlib>
#include "LoopKeys.h"
#include "LargePrime.h"

using namespace std;

bool findLoop(LargePrime*, string);


int main()
{

	//Random Test Strings
	LargePrime* testPrime1 = new LargePrime("123456789101112131415161718192021222324252627282930");
	LargePrime* testPrime2 = new LargePrime("687452123987456212365879321456687556871456321454184381484154");
	LargePrime* testPrime3 = new LargePrime("098765432112345678900987654321123456778900987654332");


	bool UIFlag = true;
	string userSeed;
	string userPrimeChoice;

	//Test "UI"
	while (UIFlag)
	{
		cout << "Prime 1 with seed 21 finds good loop" << endl;
		cout << "Enter Prime to use (TEST 1, 2, or 3): ";
		cin >> userPrimeChoice;

		if (userPrimeChoice == "3")
		{
			cout << "Enter Seed Number: ";
			cin >> userSeed;
			cout << endl;
			//check if for loops in prime
			if (findLoop(testPrime3, userSeed))
			{
				cout << endl << "Loop Found" << endl;
			}
			else
			{
				cout << endl << "No Loop Found" << endl;
			}
		}
		else if (userPrimeChoice == "2")
		{
			cout << "Enter Seed Number: ";
			cin >> userSeed;
			cout << endl;
			//check if for loops in prime
			if (findLoop(testPrime2, userSeed))
			{
				cout << endl << "Loop Found" << endl;
			}
			else
			{
				cout << endl << "No Loop Found" << endl;
			}
		}
		else if (userPrimeChoice == "1")
		{
			cout << "Enter Seed Number: ";
			cin >> userSeed;
			cout << endl;
			//check if for loops in prime
			if (findLoop(testPrime1, userSeed))
			{
				cout << endl << "Loop Found" << endl;
			}
			else
			{
				cout << endl << "No Loop Found" << endl;
			}
		}
		else if (userPrimeChoice == "q" || userPrimeChoice == "Q")
		{
			UIFlag = false;
		}
		else
		{
			cout << "Invalid Input" << endl << endl;
		}
		cout << endl << endl;
	}




	return 0;
}

//Search Prime number for a loop
bool findLoop(LargePrime* currentPrime, string seed)
{
	//flags for if a key or loop itself is found
	bool loopFound = false;
	bool keyFound = false;

	//initialize keys object
	LoopKeys* keys = new LoopKeys();

	//set current key to the seed
	string currentKey = seed;
	
	//initialize Current Index that is being searched
	int currentIndex = 0;

	//loop while a loop is not found or whole prime is searched through
	while (!loopFound && currentIndex < currentPrime->getSize())
	{
		//reinitialize key found flag and current index for inner loop
		keyFound = false;
		currentIndex = 0;

		//loop through until a key is found or the while prime has been searched
		while (!keyFound && currentIndex < currentPrime->getSize())
		{
			//Check if current key matches substring of prime at current index
			if (currentKey == currentPrime->subStringAt(currentIndex, currentKey.length()))
			{
				//check if current Key is already in the set of keys  
				if (keys->keyMatch(currentKey))
				{
					cout << "Loop Found On Key: " << currentKey << endl << endl;
					//set key values in key object
					keys->addKey(currentKey);
					keys->setLoopStart(currentKey);
					//set off loop glags
					loopFound = true;
					keyFound = true;
				}
				else
				{
					//set new key in key objects
					keys->addKey(currentKey);
					//set new key
					currentKey = to_string(currentIndex);
					//set off inner loop flag
					keyFound = true;
				}
			}
			currentIndex++;
		}
	}
	
	
	//check if loop was found or not and return accordingly after outputting keys
	if (loopFound)
	{
		keys->outPutKeys();
		delete keys;
		return true;
	}
	keys->addKey(currentKey);
	keys->outPutKeys();
	delete keys;
	return false;





}