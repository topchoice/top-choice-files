/*
Daniel Vadala
3/5/18
LoopKeys.h
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.
*/
#include <string>
#include <iostream>

using namespace std;

#pragma once
class LoopKeys
{
public:
	LoopKeys();
	~LoopKeys();
	//add key to array
	void addKey(string);

	//check if keys match
	bool keyMatch(string);
	
	//display keys to user
	void outPutKeys();
	
	//set the start of the loop if found
	void setLoopStart(string start) { loopStart = start; }


private:

	//Stores keys(temporarily an array)
	string keys[50];

	//store start of loop
	string loopStart;

	//number of keys found
	int numKeys;

	
};

