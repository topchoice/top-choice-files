/*
Daniel Vadala
3/5/18
main.cpp
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.
*/
#include <iostream>
#include <cstdlib>
#include "LoopKeys.h"
#include "LargePrime.h"
#include<string>
#include<map>
#include "getpost.h"
#include<cstring>
#include<fstream>

using namespace std;


bool findLoop(LargePrime*, string, string);

const int LOOP_CUTOFF = 100;


int main()
{

	cout << "Content-type: text/html\n\n" << endl;
	cout << "<html>" << endl << "<head><title>Loop Finder: Home</title><link rel=\"stylesheet\" type=\"text/css\" href=\"StyleSheet.css\"></head>" << endl << "<body>" << endl;
	cout << "<div class=\"header\"><h1>Loop Finder</h1></div>" << endl;
	cout << "<div class=\"topnav\"><a href=\"index.cgi?\" class=\"current\">Home</a><a href=\"about.htm\">About</a><a href=\"works.htm\">How it Works</a><a href=\"primes.htm\">The Primes</a></div>" << endl;
	cout << "<h2>Enter a seed number and choose a prime number to get started!</h2>" << endl;

	//Random Test Strings
	//LargePrime* testPrime1 = new LargePrime("pi1000000.txt");
	//LargePrime* testPrime2 = new LargePrime("e.2mil.txt");
	//LargePrime* testPrime3 = new LargePrime("M77232917.txt");

	//LargePrime* dropPrimes[] = {testPrime1, testPrime2, testPrime3};
	string fileNames[] = { "pi1000000.txt","e.2mil.txt","M756839.txt", "M859433.txt", "M1257787.txt",
		"M2976221.txt", "M3021377.txt", "M6972593.txt", "M13466917.txt", "M20996011.txt",
		"M24036583.txt", "M25964951.txt", "M30402457.txt", "M32582657.txt", "M37156667.txt", "M42643801.txt",
		"M43112609.txt", "M57885161.txt", "M74207281.txt", "M77232917.txt" };

	string numberNames[] = { "1 million digits of pi","2 million digits of e","2^756,839 - 1", "2^859,433 - 1", "2^1,257,787 - 1",
		"2^2,976,221 - 1", "2^3,021,377 - 1", "2^6,972,593 - 1", "2^13,466,917 - 1", "2^20,996,011 - 1",
		"2^24,036,583 - 1", "2^25,964,951 - 1", "2^30,402,457 - 1", "2^32,582,657 - 1", "37,156,667 - 1", "2^42,643,801 - 1",
		"2^43,112,609 - 1", "2^57,885,161 - 1", "2^74,207,281 - 1", "2^77,232,917 - 1" };

	map<string, string> Get;
	initializeGet(Get); //notice that the variable is passed by reference!
	/*cout << "Content-type: text/html\n" << endl;
	cout << "<html>" << endl << "<head><title>Loop Finder: Home</title><link rel=\"stylesheet\" type=\"text/css\" href=\"StyleSheet.css\"></head>" << endl << "<body>" << endl;
	cout << "<div class=\"header\"><h1>Loop Finder</h1></div>" << endl;
	cout << "<div class=\"topnav\"><a href=\"index.cgi?\">Home</a><a href=\"about.htm\">About</a><a href=\"works.htm\">How it Works</a><a href=\"primes.htm\">The Primes</a></div>" << endl;
	cout << "<h2>Enter a seed number and choose a prime number to get started!</h2>" << endl;*/
	cout << "<form method=\"get\">" << endl;
	cout << "<div class=\"col-1\"> <label for=\"seed\"><b>Seed Number:</b></label><br>" << endl;
	cout << "Max length: 10 digits<br>" << endl;
	cout << " <input type=\"text\" name=\"seed\" id=\"seed\"></div><br>" << endl;
	cout << "<div class=\"col-2\"> <label for=\"prime\"><b>Number to search:</b></label><br>" << endl;
	cout << "<select id=\"pri\" name=\"prime\"><option value=\"0\">One Million Digits of pi</option><option value=\"1\">Two Million Digits of e</option>" <<
		"<option value=\"2\">2^756,839 - 1</option><option value=\"3\">2^859,433 - 1</option><option value=\"4\">2^1,257,787 - 1</option>" <<
		"<option value=\"5\">2^2,976,221 - 1</option><option value=\"6\">2^3,021,377 - 1</option>" <<
		"<option value=\"7\">2^6,972,593 - 1</option><option value=\"8\">2^13,466,917 - 1</option><option value=\"9\">2^20,996,011 - 1</option>" <<
		"<option value=\"10\">2^24,036,583 - 1</option><option value=\"11\">2^25,964,951 - 1</option><option value=\"12\">2^30,402,457 - 1</option>" <<
		"<option value=\"13\">2^32,582,657 - 1</option><option value=\"13\">2^37,156,667 - 1</option><option value=\"15\">2^42,643,801 - 1</option>" <<
		"<option value=\"16\">2^43,112,609 - 1</option><option value=\"17\">2^57,885,161 - 1</option><option value=\"18\">2^74,207,281 - 1</option>" <<
		"<option value=\"19\">2^77,232,917 - 1</option>" <<	
		"</select></div><br>" << endl;
	cout << "<div class=\"col-3\"><input type=\"submit\" value=\"Find Loop!\" /></div>" << endl;
	cout << "</form><br />" << endl;
	/*cout << "<br><div id=\"resultsB\" class=\"resultsB\"><button type=\"button\" id=\"resultsB\">Save Results</button></div><br>" << endl;
	cout << "<script>document.getElementById(\"resultsB\").disabled = true;</script>" << endl;*/


	//bool UIFlag = true;
	string userSeed;
	string strPrimeChoice;
	int userPrimeChoice;

	if (Get.find("seed") != Get.end() && Get.find("prime") != Get.end() && Get["seed"] != "" && Get["seed"].length() < 10) {

		userSeed = Get["seed"];
		strPrimeChoice = Get["prime"];
		userPrimeChoice = stoi(strPrimeChoice);

		cout << "<center><img id=\"test\" src=\"\"></center>" << endl;
		cout << "<script>document.getElementById(\"test\").src = \"loading.gif\";</script>" << endl;

		/*cout << "<script>document.getElementById(\"resultsB\").disabled = false;</script>" << endl;*/

		cout << "<div id=\"results\" class=\"results\"><b>Results:</b><br><br>";
		cout << "Your seed: " << userSeed << "<br>Number searched: " << numberNames[userPrimeChoice] << "<br>";


		LargePrime* currentPrime = new LargePrime(fileNames[userPrimeChoice]);
		
		if (findLoop(currentPrime, strPrimeChoice, userSeed))
		{
			//cout << endl << "<br>Loop Found" << endl;
		}
		else
		{
			//cout << endl << "<br>No Loop Found" << endl;
		}



		//cout << "<div class=\"results\">Results: <br>
			//Seed: " << seed << "<br>Prime:" << prime << "<br>Calculation:" << seed * prime << "</div>" << endl;
	}
	else {

		if(Get.find("seed") != Get.end() && Get["seed"] == "")
		{
			cout << "<div class=\"results\"><b>Results:</b><br><br>Please enter a seed number</div>" << endl;
		}
		else if(Get["seed"].length() > 10)
		{
			cout << "<div class=\"results\"><b>Results:</b><br><br>Please enter a seed number less than 10 digits long</div>" << endl;
		}
		else
		{
			cout << "<div class=\"results\"><b>Results:</b><br><br></div>" << endl;
		}

	}

	cout << "</div>" << endl;

	//cout << "<script>alert(document.getElementById(\"results\").innerHTML)</script>" << endl;


	//out << "<script>document.getElementById(\"resultsB\").onclick = function(){alert(document.getElementById(\"results\").innerHTML);}</script>" << endl;

	/*if(!output_file){
		cout << "<script>alert(\"File error\");</script>" << endl;
	}*/

	cout << "</body></html>" << endl;
	
	//output_file.close();

	return 0;
}

//Search Prime number for a loop
bool findLoop(LargePrime* currentPrime, string userPrime, string seed)
{
	




	//flags for if a key or loop itself is found
	bool loopFound = false;
	bool keyFound = false;
	int numKeys = 1;

	//initialize keys object
	LoopKeys* keys = new LoopKeys();

	//set current key to the seed
	string currentKey = seed;
	
	//initialize Current Index that is being searched
	int currentIndex = 1;

	int currentKeyFirstDigit;

	//loop while a loop is not found or whole prime is searched through
	while ((!loopFound && currentIndex < currentPrime->getSize()) && numKeys < LOOP_CUTOFF)
	{
		//reinitialize key found flag and current index for inner loop
		keyFound = false;
		currentIndex = 1;

		//loop through until a key is found or the while prime has been searched
		while (!keyFound && currentIndex < currentPrime->getSize())
		{
			
			//Test Output To Make Sure Program is running properly
			//if (currentIndex % 1000 == 0){
				//cout << currentIndex << endl;
			//}
			
			//Check if current key matches substring of prime at current index
			currentKeyFirstDigit = currentKey[0] - '0';
			//to skip getting substring if first digit doesn't match
			if (currentPrime->digitAt(currentIndex) == currentKeyFirstDigit)
			{
				//cout << "<br>first digit match" << endl;
				if (currentPrime->equalSubString(currentKey, currentIndex, currentKey.length()))
				{
					//cout << "<br>Current Key: " << currentKey << endl;
					//check if current Key is already in the set of keys  
					if (keys->keyMatch(currentKey))
					{
						cout << "<script>document.getElementById(\"test\").src = \"\";</script>" << endl;
						cout << "<br>Loop Found! Loop begins on key " << currentKey << endl << endl;
						//set key values in key object
						keys->addKey(currentKey);
						keys->setLoopStart(currentKey);
						//set off loop glags
						loopFound = true;
						keyFound = true;
					}
					else
					{
						//set new key in key objects
						keys->addKey(currentKey);
						numKeys++;
						//set new key
						currentKey = to_string(currentIndex);
						//set off inner loop flag
						keyFound = true;
					}
				}

			}
			
			currentIndex++;
		}
	}
	
	
	//check if loop was found or not and return accordingly after outputting keys
	if (loopFound)
	{
		cout << "<script>document.getElementById(\"test\").src = \"\"; document.getElementById(\"seed\").value = \"" + seed +"\"; document.getElementById(\"pri\").selectedIndex = \"" + userPrime + "\";</script>" << endl;
		cout << "<br><br>Keys Searched Before Loop:" << endl;
		keys->outPutKeys();
		delete keys;
		return true;
	}
	cout << "<script>document.getElementById(\"test\").src = \"\"; document.getElementById(\"seed\").value = \"" + seed +"\"; document.getElementById(\"pri\").selectedIndex = \"" + userPrime + "\";</script>" << endl;
	cout << "<br>Loop Not Found!<br><br>Keys Searched Before Failure:" << endl;
	keys->addKey(currentKey);
	keys->outPutKeys();
	delete keys;
	return false;

}
