/*
Daniel Vadala
3/5/18
LargePrime.cpp
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.
*/
#include "LargePrime.h"


//constructor sets digits of LargePrime
LargePrime::LargePrime(string fileName)
{
	//digits = newDigits;
	//size = digits.length();
	readFile(fileName);

}


LargePrime::~LargePrime()
{
}

//Given an index and size returns substring at that index and returns substring of the size
string LargePrime::subStringAt(int index, int size)
{
	string pieceOfPrime = "";

	for (int i = index; i < index + size; i++)
	{
		pieceOfPrime += to_string(arrayDigits[i]);
	}

	return pieceOfPrime;
}

void LargePrime::readFile(string fileName)
{
	arrayDigits[0] = 0;
	ifstream digitsFile;
	char fileDigit;
	int counter = 1;
	cout << fileName << endl;
	digitsFile.open(fileName);
	if (!digitsFile) 
	{
		cout << "ERROR" << endl;
	}
	else
	{
		while (digitsFile >> fileDigit)
		{
			
			if (isdigit(fileDigit))
			{
				arrayDigits[counter] = fileDigit - '0';
				counter++;
			}

		}
		size = counter;
		cout << "PRIME SIZE: " << size << endl;
	}
}
