/*
Daniel Vadala
3/5/18
LoopKeys.cpp
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.

 This file is part of LoopFinder.

    LoopFinder is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LoopFinder is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LoopFinder.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "LoopKeys.h"
#include<cstring>
#include <iostream>
#include <cstdlib>
#include "LargePrime.h"
#include<string>
#include<map>
#include<fstream>
using namespace std;

// ZAK: Not needed -- causing double definitions.
//#include "getpost.h"


//initializes number of keys to 0 and loop start to N/A
LoopKeys::LoopKeys()
{
	numKeys = 0;
	loopStart = "N/A";
}


LoopKeys::~LoopKeys()
{
}

//adds keys to keys array
void LoopKeys::addKey(string newKey)
{
	keys[numKeys] = newKey;
	numKeys++;
}

//loops through keys to find match
bool LoopKeys::keyMatch(string newKey)
{
	for (int i = 0; i < numKeys; i++)
	{
		if (newKey == keys[i])
		{
			return true;
		}
	}
	return false;
}

//outputs keys found in prime number
void LoopKeys::outPutKeys()
{

	//flag to show start of loop
	bool showStart = false;

	//loop through printing keys found
	for (int i = 0; i < numKeys; i++)
	{
		//check where loop starts and output to user
		if (!showStart && keys[i] == loopStart)
		{
			showStart = true;
			cout << endl << "<br><br>Loop Starts Here:" << endl;
			cout << "<br>Key " << i+1 << ": " << keys[i] << endl;
		}
		else
		{
			cout << "<br>Key " << i+1 << ": " << keys[i] << endl;
		}
	}
}
