/*
Daniel Vadala
3/5/18
LoopKeys.h
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.

 This file is part of LoopFinder.

    LoopFinder is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LoopFinder is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LoopFinder.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string>
#include <iostream>

using namespace std;

#pragma once
class LoopKeys
{
public:
	LoopKeys();
	~LoopKeys();
	//add key to array
	void addKey(string);

	//check if keys match
	bool keyMatch(string);
	
	//display keys to user
	void outPutKeys();
	
	//set the start of the loop if found
	void setLoopStart(string start) { loopStart = start; }


private:

	//Stores keys(temporarily an array)
	string keys[100];

	//store start of loop
	string loopStart;

	//number of keys found
	int numKeys;

	
};

