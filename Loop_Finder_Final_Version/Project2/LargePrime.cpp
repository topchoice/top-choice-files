/*
Daniel Vadala
3/5/18
LargePrime.cpp
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.

 This file is part of LoopFinder.

    LoopFinder is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LoopFinder is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LoopFinder.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "LargePrime.h"


//constructor sets digits of LargePrime
LargePrime::LargePrime(string fileName)
{

	readFile(fileName);

}


LargePrime::~LargePrime()
{
}

//Given an index and size returns substring at that index and returns substring of the size
string LargePrime::subStringAt(int index, int size)
{
	string pieceOfPrime = "";

	for (int i = index; i < index + size; i++)
	{
		pieceOfPrime += to_string(arrayDigits[i]);
	}
	
	return pieceOfPrime;
}

//check if key is equal to a substring
bool LargePrime::equalSubString(string currentKey, int index, int size)
{
	int currentKeyCounter = 0;
	for (int i = index; i < index + size; i++)
	{

		if (currentKey[currentKeyCounter] - '0' != arrayDigits[i])
		{
			return false;
		}

		currentKeyCounter++;
	}
	return true;
}




void LargePrime::readFile(string fileName)
{

	arrayDigits[0] = 0;
	ifstream digitsFile;
	char fileDigit;
	int counter = 1;

	digitsFile.open(fileName);
	if (!digitsFile) 
	{
		cout << "ERROR" << endl;
	}
	else
	{
		while (digitsFile >> fileDigit)
		{
			
			if (isdigit(fileDigit))
			{
				arrayDigits[counter] = fileDigit - '0';
				counter++;
			}

		}
		size = counter;

	}
}
