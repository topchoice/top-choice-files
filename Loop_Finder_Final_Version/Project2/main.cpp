/*
Daniel Vadala
3/5/18
main.cpp
LoopFinderEngine: Starter code for Loop Finder Program created for Professor Duston by Joanna and Daniel.
Program tests start for brute force of loop finder on smaller numbers.

 This file is part of LoopFinder.

    LoopFinder is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LoopFinder is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LoopFinder.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <cstdlib>
#include "LoopKeys.h"
#include "LargePrime.h"
#include<string>
#include<map>
#include "getpost.h"
#include<cstring>
#include<fstream>

using namespace std;


bool findLoop(LargePrime*, string, string);

const int LOOP_CUTOFF = 100;


int main()
{
	//output the HTML
	cout << "Content-type: text/html\n\n" << endl;
	cout << "<html>" << endl << "<head><title>Loop Finder: Home</title><link rel=\"stylesheet\" type=\"text/css\" href=\"StyleSheet.css\"></head>" << endl << "<body>" << endl;
	cout << "<div class=\"header\"><h1>Loop Finder</h1></div>" << endl;

	//navigation bar
	cout << "<div class=\"topnav\"><a href=\"loop.cgi?\" class=\"current\">Home</a><a href=\"about.htm\">About</a><a href=\"works.htm\">How it Works</a><a href=\"primes.htm\">The Primes</a><a href=\"help.htm\">Help</a></div>" << endl;
	cout << "<h2>Enter a seed number and choose a prime number to get started!</h2>" << endl;

	//array to store the names of the prime text files
	string fileNames[] = { "pi1000000.txt","e.2mil.txt","M756839.txt", "M859433.txt", "M1257787.txt",
		"M2976221.txt", "M3021377.txt", "M6972593.txt", "M13466917.txt", "M20996011.txt",
		"M24036583.txt", "M25964951.txt", "M30402457.txt", "M32582657.txt", "M37156667.txt", "M42643801.txt",
		"M43112609.txt", "M57885161.txt", "M74207281.txt", "M77232917.txt" };

	//array to store the prime drop down menu labels
	string numberNames[] = { "1 million digits of pi","2 million digits of e","2^756,839 - 1", "2^859,433 - 1", "2^1,257,787 - 1",
		"2^2,976,221 - 1", "2^3,021,377 - 1", "2^6,972,593 - 1", "2^13,466,917 - 1", "2^20,996,011 - 1",
		"2^24,036,583 - 1", "2^25,964,951 - 1", "2^30,402,457 - 1", "2^32,582,657 - 1", "37,156,667 - 1", "2^42,643,801 - 1",
		"2^43,112,609 - 1", "2^57,885,161 - 1", "2^74,207,281 - 1", "2^77,232,917 - 1" };

	map<string, string> Get;
	initializeGet(Get); 

	//form for user input
	cout << "<form method=\"get\">" << endl;

	//seed number label
	cout << "<div class=\"col-1\"> <label for=\"seed\"><b>Seed Number:</b></label><br>" << endl;
	cout << "Max length: 10 digits<br>" << endl;

	//seed number text field
	cout << " <input type=\"text\" name=\"seed\" id=\"seed\"></div><br>" << endl;

	//drop down menu label
	cout << "<div class=\"col-2\"> <label for=\"prime\"><b>Number to search:</b></label><br>" << endl;

	//prime to search drop down menu
	cout << "<select id=\"pri\" name=\"prime\"><option value=\"0\">One Million Digits of pi</option><option value=\"1\">Two Million Digits of e</option>" <<
		"<option value=\"2\">2^756,839 - 1</option><option value=\"3\">2^859,433 - 1</option><option value=\"4\">2^1,257,787 - 1</option>" <<
		"<option value=\"5\">2^2,976,221 - 1</option><option value=\"6\">2^3,021,377 - 1</option>" <<
		"<option value=\"7\">2^6,972,593 - 1</option><option value=\"8\">2^13,466,917 - 1</option><option value=\"9\">2^20,996,011 - 1</option>" <<
		"<option value=\"10\">2^24,036,583 - 1</option><option value=\"11\">2^25,964,951 - 1</option><option value=\"12\">2^30,402,457 - 1</option>" <<
		"<option value=\"13\">2^32,582,657 - 1</option><option value=\"13\">2^37,156,667 - 1</option><option value=\"15\">2^42,643,801 - 1</option>" <<
		"<option value=\"16\">2^43,112,609 - 1</option><option value=\"17\">2^57,885,161 - 1</option><option value=\"18\">2^74,207,281 - 1</option>" <<
		"<option value=\"19\">2^77,232,917 - 1</option>" <<	
		"</select></div><br>" << endl;

	//submit button
	cout << "<div class=\"col-3\"><input type=\"submit\" value=\"Find Loop!\" /></div>" << endl;
	cout << "</form><br />" << endl;

	//variables to store the user input
	string userSeed;
	string strPrimeChoice;
	int userPrimeChoice;

	//if the user entered valid input
	if (Get.find("seed") != Get.end() && Get.find("prime") != Get.end() && Get["seed"] != "" && Get["seed"].length() <= 10) {

		userSeed = Get["seed"];
		strPrimeChoice = Get["prime"];
		userPrimeChoice = stoi(strPrimeChoice);

		cout << "<center><img id=\"test\" src=\"\"></center>" << endl;
		cout << "<script>document.getElementById(\"test\").src = \"loading.gif\";</script>" << endl;

		//results section
		cout << "<div id=\"results\" class=\"results\"><b>Results:</b><br><br>";
		cout << "Your seed: " << userSeed << "<br>Number searched: " << numberNames[userPrimeChoice] << "<br>";


		LargePrime* currentPrime = new LargePrime(fileNames[userPrimeChoice]);
		
		if (findLoop(currentPrime, strPrimeChoice, userSeed))
		{
			//Loop Found
		}
		else
		{
			//No Loop Found
		}
		delete currentPrime;

	}
	else {

		//invalid input error messages
		if(Get.find("seed") != Get.end() && Get["seed"] == "")
		{
			cout << "<div class=\"results\"><b>Results:</b><br><br>Please enter a seed number</div>" << endl;
		}
		else if(Get["seed"].length() > 10)
		{
			cout << "<div class=\"results\"><b>Results:</b><br><br>Please enter a seed number which is 10 digits or less</div>" << endl;
		}
		else
		{
			cout << "<div class=\"results\"><b>Results:</b><br><br></div>" << endl;
		}

	}

	cout << "</div>" << endl;

	cout << "</body></html>" << endl;

	return 0;
}

//Search Prime number for a loop
bool findLoop(LargePrime* currentPrime, string userPrime, string seed)
{
	
	//flags for if a key or loop itself is found
	bool loopFound = false;
	bool keyFound = false;
	int numKeys = 1;

	//initialize keys object
	LoopKeys* keys = new LoopKeys();

	//set current key to the seed
	string currentKey = seed;
	
	//initialize Current Index that is being searched
	int currentIndex = 1;

	int currentKeyFirstDigit;

	//loop while a loop is not found or whole prime is searched through
	while ((!loopFound && currentIndex < currentPrime->getSize()) && numKeys < LOOP_CUTOFF)
	{
		//reinitialize key found flag and current index for inner loop
		keyFound = false;
		currentIndex = 1;

		//loop through until a key is found or the while prime has been searched
		while (!keyFound && currentIndex < currentPrime->getSize())
		{
			
			//Test Output To Make Sure Program is running properly
			//if (currentIndex % 1000 == 0){
				//cout << currentIndex << endl;
			//}
			
			//Check if current key matches substring of prime at current index
			currentKeyFirstDigit = currentKey[0] - '0';
			//to skip getting substring if first digit doesn't match
			if (currentPrime->digitAt(currentIndex) == currentKeyFirstDigit)
			{
				//cout << "<br>first digit match" << endl;
				if (currentPrime->equalSubString(currentKey, currentIndex, currentKey.length()))
				{
					//cout << "<br>Current Key: " << currentKey << endl;
					//check if current Key is already in the set of keys  
					if (keys->keyMatch(currentKey))
					{
						cout << "<script>document.getElementById(\"test\").src = \"\";</script>" << endl;
						cout << "<br>Loop Found! Loop begins on key " << currentKey << endl << endl;
						//set key values in key object
						keys->addKey(currentKey);
						keys->setLoopStart(currentKey);
						//set off loop glags
						loopFound = true;
						keyFound = true;
					}
					else
					{
						//set new key in key objects
						keys->addKey(currentKey);
						numKeys++;
						//set new key
						currentKey = to_string(currentIndex);
						//set off inner loop flag
						keyFound = true;
					}
				}

			}
			
			currentIndex++;
		}
	}
	
	
	//check if loop was found or not and return accordingly after outputting keys
	if (loopFound)
	{
		//output results
		cout << "<script>document.getElementById(\"test\").src = \"\"; document.getElementById(\"seed\").value = \"" + seed +"\"; document.getElementById(\"pri\").selectedIndex = \"" + userPrime + "\";</script>" << endl;
		cout << "<br><br>Keys Searched Before Loop:" << endl;
		keys->outPutKeys();
		delete keys;
		return true;
	}
	cout << "<script>document.getElementById(\"test\").src = \"\"; document.getElementById(\"seed\").value = \"" + seed +"\"; document.getElementById(\"pri\").selectedIndex = \"" + userPrime + "\";</script>" << endl;
	cout << "<br>Loop Not Found!<br><br>Keys Searched Before Failure:" << endl;
	keys->addKey(currentKey);
	keys->outPutKeys();
	delete keys;
	return false;

}
